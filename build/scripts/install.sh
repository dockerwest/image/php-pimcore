#!/bin/sh

set -e

# ffmpeg comes from backports
echo "deb http://deb.debian.org/debian $(lsb_release -sc)-backports main" \
    > /etc/apt/sources.list.d/backports.list

# install packages
apt-get update

extensions -i bz2 gd imagick intl mbstring memcached mysql redis soap \
    sodium sqlite3 xml zip
apt-get install -y imagemagick inkscape pngnq pngcrush xvfb cabextract \
    poppler-utils xz-utils libreoffice libreoffice-math jpegoptim ffmpeg \
    html2text ghostscript exiftool xfonts-75dpi wkhtmltopdf

apt-get clean -y

rm -rf /usr/share/man/man1/

# update permissions to allow rootless operation
/usr/local/bin/permissions
